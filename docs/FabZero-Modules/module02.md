# 2. Conception Assistée par Ordinateur

Cette semaine j'ai travaillé sur deux axes : 
- La modélisation 3D avec Fusion 360
- Développer une idée plus claire de son projet d'objet.


En commençant par le dessiner et/ou modéliser en 3D, on saisit plus distinctement quels sont les enjeux et contraintes de son objet.
C'est donc ce que je vais faire cette semaine, modéliser une chaise sur fusion. La chaise est un objet utilisée au quotidien depuis des milliers d'années quasiment partout dans le monde, il en existe donc évidemment une grande variété, à la fois de forme, de matériau, d'usage, ...

Je vais travailler à la modélisation d'un type de chaise assez standardisé, qui est la chaise sur laquelle j'ai passé des années complètes : la chaise scolaire. 
Cette assise présente l'avantage de pouvoir être empilée, ce qui lui à permis d'intégrer la quasi totalités des établissement scolaire. Cette particularité implique une conception qui néglige le confort, entrainant souvent des problèmes de dos, qui rappelons le est la première dépense de la sécurité sociale française. 

Je reviendrai sur l'histoire des assises et son impact plus tard, je vais donc modéliser une chaise scolaire typique empilable, afin de comprendre encore mieux cet objet tant utilisé et si peu requestionné.


## Fusion

Fusion est un logiciel de CAO (conception assistée par ordinateur), il permet de concevoir des modèles 3D à partir d'esquisse 2D. Le logiciel diffère des logiciels que j'utilise habituellement en architecture, il est conçu pour le design d'objets et non de bâtiments, on va surtout se baser sur les esquisses et ensuite créer le 3D. 
Sur Sketchup par exemple on a plus l'aspect pâte à modeler, on retravaille beaucoup la forme 3D, ce que je trouve très agréable pour la recherche par empirisme. 
C'est moins facile sur Fusion, mais ce n'est pas son but.
Je vais donc devoir revoir la façon de créer, quels sont les étapes à suivre, un même objet pouvant être modélisé de beaucoup de manière différentes.  
Au cours de cette semaine et de la modélisation qui va suivre j'ai eu beaucoup de mal à m'adapter à cette nouvelle façon de modéliser, mais en contrepartie le logiciel permet une grande facilité pour modéliser des fonctions comme des courbes, des révolutions, ..., une grande quantité de forme peu présentes en architecture qui sont communes en Design objet. De plus le logiciel intègre des outils comme la matière et les rendus très qualitatifs qui semblent  de qualité professionnelle. 


Je commence avec la création d'une esquisse 2D de la barre de métal qui compose le pied avant, la structure de l'assise et du dossier. J'utilise la fonction CRÉER UNE ESQUISSE, qui m'ouvre un espace 2D sur lequel on peut dessiner facilement (un avantage de fusion est de pouvoir facilement/intuitivement passer de vues 2D à 3D).
J'utilise la fonction LIGNE pour tracer l'esquisse, je trace les 3 lignes que je relis ensemble par la fonction CONGÉ. Dans les 2 cas j'écris manuellement les mesures au moment de la création de la fonction, plutôt que de bouger la souris à la distance que tu désire. C'est plus précis et plus rapide, je vais tout le temps procéder de cette manière sur le logiciel.

![](../images/2-chaise-fusion-1.png)




Une fois l'esquisse réalisée je vais en créer une 3D, c'est la technique principale de ce logiciel, dessiner une 2D précise et utiliser une fonction adaptée pour créer une forme 3D, ce procédé permet de créer des modèles 3D qui seraient très complexe à modéliser directement en 3D.
Pour les pieds de chaise j'utilise la fonction TUYAU, qui correspond parfaitement au tube d'aluminium qui compose ces chaises. J'encode le diamètre du tube ainsi que son épaisseur.




Je vais créer le second pied de chaise, c'est le même que le premier. J'utilise la fonction DÉPLACER afin de placer précisément le second pied. Je coche la case COPIER afin que la pièce déplacée soit une copie donc que le premier pied reste bien à sa place. Procéder en utilisant la fonction déplacer est plus efficace que de copier coller la pièce puis de la déplacer, car on aura un origine du déplacement pas pratique pour encoder les distances de déplacement. 

![](../images/2-chaise-fusion-2.png)


Je modélise ensuite les deux pieds arrières en une seule pièce, avec le même procédé d'esquisse et de fonction tube que les pieds précédents.
Après de multiples essais je comprend que la meilleure façon de placer des objets créés est de le faire en amont : dessiner la pièce 2D directement là où elle est dans le modèle réel, ce qui est différent de Sketchup par exemple où je créais la pièce puis je la déplaçait où elle devrait être. 
Je crée mon esquisse sur un plan préalablement créé et placé au bon endroit, pour créer ce plan j'utilise la fonction PLAN DE DÉCALAGE.
 
C'est vraiment contre intuitif pour moi et c'est surement ce que je trouve de moins bien fait sur ce logiciel les déplacements d'objets les uns par rapport aux autres, c'est ce qui m'a fait perdre le plus de temps lors de la conception de la chaise. Je dois m'habituer à cette façon de procéder que j'aime peu car très difficile de faire des essais directement en 3D pour tâtonner, qui est pour moi une étape importante lors d'une conception, tester rapidement plusieurs modèles et leurs implications visuelles et techniques.

Une fois l'esquisse créé, je répète la fonction TUYAU, pour reproduire le principe d'encastrement par soudure j'ai volontairement placé la pièce de façon à ce qu'elle soit en contact avec les autres pieds. Je dois utiliser la fonction COMBINER afin de lier ces pièces entres elles.

![](../images/2-chaise-fusion-3.png)

Je répète le processus précédent de création de nouvelle esquisse sur un plan préalablement bien placé, je dessine ce qui sera la pièce de l'assise en bois de la chaise. 
Une fois l'esquisse tracée j'utilise la fonction EXTRUSION afin de donner du volume à mon esquisse, j'encode l'épaisseur.
Cette fonction est la plus communément utilisée du logiciel car elle permet simplement de donner une épaisseur à une surface plane comme une esquisse.
De multiples paramètres sont présent dans cette fonction comme l'extrusion des deux côtés, démarrer avec un décalage...
(Noter que l'assise est déjà texturé bois, j'explique ultérieurement comment j'ai procédé pour appliquer cette texture)

![](../images/2-chaise-fusion-4.png)


Je vais ensuite modéliser la forme de courbe du bout de l'assise qui augmente son confort. Pour ce genre d'opération il existe une multitudes de techniques, je vais utiliser la fonction RÉVOLUTION, il suffit de choisir une surface qui va effectuer une rotation/révolution autour d'un axe que l'on précise, ce sera une technique possible pour créer un cylindre ou un tube par  exemple.
On à la possibilité de régler l'angle de la forme extrudée, 360° étant un tour complet, je définis mon angle à 65°. 
J'applique des chanfreins à la pièce de l'assise, je détaille cette fonction plus loin dans la présentation de la démarche.

![](../images/2-chaise-fusion-5.png)

Pour le dossier je procède comme on à vu précédemment avec la création d'une esquisse puis extrusion, je me penche sur la texture cette fois-ci, je n'ai pas totalement saisi les composant, groupes, ... je ne pense pas que la façon dont je procède soit la meilleure mais cela permet au moins de montrer la fonction APPARENCE. Cette fonction est obtenue avec le raccourcis clavier a, ou en sélectionnant les objets/corps/composant puis clic droit et APPARENCE.
Le logiciel intègre par défaut une palette de textures assez fournies, certaines sont téléchargeable en un clic. Il est donc très rapide de texturer des éléments du modèle, de plus ces textures sont de très bonne qualité et offre un bon rendu.
 
![](../images/2-chaise-fusion-7.png)     ![](../images/2-chaise-fusion-8.png)




Je modélise maintenant les gommes de plastique qui se placent sur les extrémités des structures métalliques. 
Je vais utiliser la fonction CONGÉ sur l'arrête de la pièce de caoutchouc pour lui donner sa forme arrondie, cette fonction permet d'adoucir les angles avec une coupe courbe, un effet comme poncé au papier de verre par exemple.


![](../images/2-chaise-fusion-10.png)           ![](../images/2-chaise-fusion-11.png)



Ensuite il faut déplacer la pièce au bout des tubes, c'est encore une fois pas très pratique pour placer les pieds avec la fonction déplacer, il faut vraiment avoir en tête toutes les mesures comme l'espacement des pieds. Mais pour les extrémités des tubes du dossier, il y a un angle donc je ne pas utiliser cette méthode, après pas mal d'essais infructeux je trouve finalement une fonction qui est adaptée : ALIGNER, cette fonction permet de déplacer un objet en alignant sa géométrie avec celle d'une autre pièce. J'encode donc le contour de la pièce de caoutchouc au contour du tube. La pièce est bien placée, cette méthode est très efficace et va sûrement régler une bonne partie de mes problèmes de placement des objets les uns par rapport aux autres.

![](../images/2-chaise-fusion-13.png)     ![](../images/2-chaise-fusion-14.png)



Le modèle est fini, il y sûrement quelques erreurs, dont une partie que j'ai corrigé en revoyant le modèle complètement une seconde fois pour l'écriture de ce module 2. AU final le logiciel permet un résultat de bonne qualité de rendu, tout en étant effectué correctement. Le principe de l'historique est génial car il permet des trouver des erreurs dans le projet qui se sont glissée au fil de la modélisation, se vérifier, retravailler des parties du projet, c'est un outil très pratique pour avoir un modèle à la fois esthétique et surtout fonctionnel. 

La dernière étape que je présente se passe dans la partie non plus CONCEPTION mais RENDU du logiciel, après avoir placé dans la vue désirée l'objet, j'utilise la fonction RENDU DANS LE CANEVAS, le logiciel crée un rendu 3D d'une très bonne qualité basée sur les textures appliquées préalablement.

![](../images/2-chaise-fusion-15.png)


En conclusion je dirais que ce logiciel est intuitif et plutôt facile à prendre en main, et les créations que l'on fait dessus de qualité, qui ne pourrait être atteinte dans les logiciels que j'utilisais précédemment.




## Référence de modèles 


- [Google](https://www.techni-contact.com/produits/1276-2531240-chaise-scolaire-en-hetre-empilable.html)


